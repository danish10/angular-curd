import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {RouterModule,Routes } from '@angular/router'

import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { ProfileComponent } from './profile/profile.component';
import { ModalModule } from 'ngx-bootstrap/modal';
import{ ReactiveFormsModule} from '@angular/forms';
import { UserDetailComponent } from './user-detail/user-detail.component'
const routes: Routes=[
  {
  path:'login',component:LoginComponent
},
{
  path:'profile',component:ProfileComponent
},
{
  path:'user',component:UserDetailComponent
}]
@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    ProfileComponent,
    UserDetailComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(routes),
    ModalModule.forRoot(),
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent],
  exports: [ RouterModule ]
})
export class AppModule { }
