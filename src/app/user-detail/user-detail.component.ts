import { Component, OnInit,TemplateRef } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-user-detail',
  templateUrl: './user-detail.component.html',
  styleUrls: ['./user-detail.component.css']
})
export class UserDetailComponent implements OnInit {
  Value: string; 
  submitted:boolean=false;
  update:boolean=false;
  mobnumPattern = "^((\\+91-?)|0)?[0-9]{10}$";
  registerForm: FormGroup
  modalRef: BsModalRef;
  user:any={
    name:'',
    address:'',
    mobile:'',
  }
  
  userDetails=[];
  updatedItem: any;
  constructor() { }

  ngOnInit() {
  }

}
