import { Component, OnInit, TemplateRef } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

export class MyItems {    
  Value: string;    
  constructor(Value:string)    
  {    
    this.Value = Value;    
  }    
}  

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  
  Value: string; 
  submitted:boolean=false;
  update:boolean=false;
  mobnumPattern = "^((\\+91-?)|0)?[0-9]{10}$";
  registerForm: FormGroup
  modalRef: BsModalRef;
  user:any={
    name:'',
    address:'',
    mobile:'',
  }
  
  userDetails=[];
  updatedItem: any;
  mobileError: any;
  constructor(private modalService: BsModalService, private fb: FormBuilder,) {
     }

  ngOnInit() {
    this.registerForm = this.fb.group({
      name: ['', Validators.required],
      address: ['', Validators.required],
      mobile: ['', Validators.required,Validators.minLength(10),Validators.maxLength(10)]
    })
  }
  openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template);
    this.user={}
    this.update=false;
  }
  get f(){
   return this.registerForm.controls;
  }

  onSubmit(){
    // if(this.user.mobile.length > 10 || this.user.mobile.length < 10){
    //   this.mobileError='Please Enter 10 Digit mobile number'
    // }
    // this.user={}
this.submitted= true;
if(this.registerForm.invalid){
return;
}
this.userDetails.push(this.user)
this.user={}
alert('add successfull')
this.modalRef.hide();
// this.update=true
  }
  
  delete(i){
this.userDetails.splice(i,1)
  }
  updateUser(user){
   this.updatedItem = this.userDetails.find(this.findIndexToUpdate,user);
   let index = this.userDetails.indexOf(this.updatedItem);
   this.userDetails[index]=user;
  // this.update=true
  alert('update successfull');
  this.modalRef.hide();
  }
  findIndexToUpdate(user){
return user  === this
  }
  
    

    editModal(template: TemplateRef<any>,users) {
      this.modalRef = this.modalService.show(template,users);
     
      this.user=users
      this.update=true;
      
    }
    restrictNumeric(event) {
      return (event.charCode == 8 || event.charCode == 0) ? null : event.charCode >= 48 && event.charCode <= 57;
    }
}
